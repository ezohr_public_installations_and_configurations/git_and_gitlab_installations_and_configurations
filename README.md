# Git And GitLab Installations And Configurations

## Prerequisites

> Created: [Account On GitLab] ([Register For GitLab](https://gitlab.com/users/sign_up))
>
> Installed: Visual Studio Code ([Download Visual Studio Code](https://code.visualstudio.com/Download))

## Git

### macOS Terminal Application (Git)

`git --version`

> Select: Install

***

## GitLab ([Generating A New SSh Key Pair](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair))

### macOS Terminal Application (GitLab Part 1)

`ssh-keygen -t ed25519 -C "GitLab"`

> Enter: [Filename]
>
> Enter: [Password]
>
> Enter: [Password]

`cat ~/.ssh/[Filename].pub`

> Select: [Content]
>
> Copy: [Content]

### macOS Safari Application (GitLab)

> Navigate: [User] / Settings / SSh Keys ([SSh Keys](https://gitlab.com/profile/keys))
>
> Select: Key
>
> Paste: [Content]
>
> Enter: [Expiry]
>
> Select: Add Key

### macOS Terminal Application (GitLab Part 2)

`nano ~/.ssh/config`

> Enter: ([.ssh/config](https://gitlab.com/Ezohr-InstallationsAndConfigurations/GitAndGitLabInstallationsAndConfigurations/-/blob/master/.ssh/config))

```sh
# GitLab.com
Host gitlab.com
  Preferredauthentications publickey
  IdentityFile ~/.ssh/[Filename]
  AddKeysToAgent yes
  UseKeychain yes
```

> Enter: Control + x
>
> Enter: y

`ssh -T git@gitlab.com`

> Verify: [ECDSA Key Fingerprint] Is HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw ([SSh Host Keys Fingerprints](https://gitlab.com/help/user/gitlab_com/index.md#ssh-host-keys-fingerprints))
>
> Enter: Yes

`git config --global user.name "[Username]"`

`git clone ssh://git@gitlab.com/[Group]/[Project].git`

> Enter: [Password]

***

## GitLab Workflow ([GitLab VSCode Extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension))

### Visual Studio Code Application (GitLab Workflow Part 1)

> Select: [Extensions]
>
> Search: GitLab Workflow (Fatih Acet)
>
> Select: Install

### macOS Safari Application (GitLab Workflow)

> Navigate: [User] / Settings / Access Tokens ([Personal Access Tokens](https://gitlab.com/profile/personal_access_tokens))
>
> Enter: [Name]
>
> Enter: [Expiry]
>
> Select: API
>
> Select: Read User
>
> Select: Create Personal Access Token
>
> Select: [Copy Personal Access Token]
>
> Save: [Access Token]

### Visual Studio Code Application (GitLab Workflow Part 2)

> Select: Command + Shift + P
>
> Search: GitLab: Set GitLab Personal Access Token
>
> Enter: [Enter]
>
> Enter: [Access Token]
